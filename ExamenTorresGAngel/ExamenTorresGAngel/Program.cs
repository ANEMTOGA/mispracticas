﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenTorresAngel
{
    class Program
    {
        static void Main(string[] args)
        {
            int i,n,NoCliente;
            decimal Credito, Deuda, TotalDeudas=0,NumDeuda=0;
            Console.WriteLine("Cantidad de clientes a revisar");
            n = int.Parse(Console.ReadLine());
            i = n;
            n = 1;
            Console.Clear();
            do {
                Console.WriteLine("Bienvenido a este programa\nIntroduce el numero de cuenta de cliente\nCliente numero [" + n + "]");
                NoCliente = int.Parse(Console.ReadLine());
                Console.WriteLine("Introduce el limite de de credito actual");
                Credito = decimal.Parse(Console.ReadLine());
                Credito = Ayuda.Credito(Credito);
                Console.WriteLine("Introduce la deuda actual");
                Deuda = decimal.Parse(Console.ReadLine());
                TotalDeudas = Ayuda.Deuda(Credito,Deuda,TotalDeudas);
                NumDeuda = Ayuda.NumDeuda(Credito, Deuda, NumDeuda);
                //Console.WriteLine("T " + TotalDeudas + "\nN " + NumDeuda); ///corroborar las varialbles
                Console.WriteLine("Nuevo credito " + Credito);
                n = n + 1;
                Console.WriteLine("Presione cualquier tecla para continuar");
                Console.ReadKey();
                Console.Clear();
            } while (n <= i );
            Console.WriteLine("Cantidad total de deuda $" + TotalDeudas + "\nNumero total de adeudadores " + NumDeuda);
            Console.WriteLine("Presione cualquier tecla para terminar");
            Console.ReadKey();
            Console.Clear();
        }
    }
}
