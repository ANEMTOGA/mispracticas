﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Extensions;

namespace PracticasArreglos
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Elija una opcion");
            int X; 
            X = int.Parse(Console.ReadLine());
            switch (X)
            {
                case 1:
            Console.WriteLine("Bienvenido a este programa que imprime los numeros del 4 al 14\nPresiona cualquier tecla para iniciar");
                    Console.ReadKey();
                    Console.Clear();
                    Array.suma();
                    break;
                        case 2:
                    Console.WriteLine("Bienvenido a este programa que imprime los numeros de 2 en 2 dentro de los valores 1 a 100 \nPresiona cualquier tecla para iniciar");
                    Console.ReadKey();
                    Console.Clear();
                    Array.Suma2();
                    break; 
                case 3:
                    Console.WriteLine("Bienvenido a este programa que imprime los numeros divisibles entre 3 dentro de los valores 1 a 100 \nPresiona cualquier tecla para iniciar");
                    Console.ReadKey();
                    Console.Clear();
                    Array.Divisible3();

                    break;
                case 5:
                    Console.WriteLine("Bienvenido a este programa que imprime la suma de los numero pares hasta el 24 \nPresiona cualquier tecla para iniciar");
                    Console.ReadKey();
                    Console.Clear();
                    Array.Mariana();
                    break;
                case 6:
                    Console.WriteLine("Bienvenido a este programa que imprime la media de 5  numeros ingresados \nPresiona cualquier tecla para iniciar");
                    Console.ReadKey();
                    Console.Clear();
                    Array.Celic();
                    break;
                case 7:
                    Console.WriteLine("Bienvenido a este programa que imprime la suma de datos ya asignados \nPresiona cualquier tecla para iniciar");
                    Console.ReadKey();
                    Console.Clear();
                    Array.SumaSignos();
                    break;
                case 8:
                    Console.WriteLine("Bienvenido este programa cuenta las vocales que existen en una palabra \nPresiona cualquier tecla para iniciar");
                    Console.ReadKey();
                    Console.Clear();
                    Array.Vocales();
                    break;
                case 9:
                    Console.WriteLine("Bienvenido este programa compara los datos de 2 arrays \nPresiona cualquier tecla para iniciar");
                    Console.ReadKey();
                    Console.Clear();
                    Array.ArreglosIdenticos();
                    break;
                case 4:
                    Console.WriteLine("Bienvenido este programa llena un arreglo de 10 números aleatorios entre 50 y 100 y multiplica por 0.5\nPresiona cualquier tecla para iniciar");
                    Console.ReadKey();
                    Console.Clear();
                    Array.NumAlea();
                    break;
            } 
        }
    }
}